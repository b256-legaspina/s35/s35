// [SECTION] JS Server

const express = require("express");
// mongoose is a package/module that allows the creation of schemas to model our data structures and also has access to different methods for manipulating our database.
const mongoose = require("mongoose");

const app = express();

const port = 3000;

// [SECTION] MongoDB Connection

// { newUrlParser : true } allows us to avoid any current and future errors while connecting to MongoDB

mongoose.connect("mongodb+srv://admin:admin1234@b256legaspina.ssgp2cu.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Checking of connection (checking of connection only)
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// [SECTION] Mongoose Schema
// Schemas determine the structure of the document to be written in the database
// in layman's term, it acts like a blueprint of our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});

// [SECTION] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database

// Server > Schema (blueprint) > Database > Collection

// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const Task = mongoose.model("Task", taskSchema);

// Middlewares

app.use(express.json());
// The url by default, accepts only string and array data types. By extending the urlencoded, the app can now accept object data type as well.
app.use(express.urlencoded({extended: true}));

// Creating a new Task

/*
	Business Logic:
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/task", (req, res) => {
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	Task.findOne({name: req.body.name}).then((result, error) => {

		if(result !== null && result.name == req.body.name) {

			return res.send('Duplicate Task Found');
		}else {
			let newTask = new Task ({
				name: req.body.name
			});
			newTask.save().then((savedTask, savedErr) => {

				if(savedErr) {
					return console.error (savedErr);
				} else {
					return res.status(201).send("New Task Created");
				}
			})
		}

	})
})


// [SECTION] Getting All Tasks

 // Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) => {

	Task.find({}).then((result, err) => {

		if(err) {

			return console.error(err)

		} else {
			res.status(200).json({
				data: result
			})
		}
	})
})


// Activity

// USER SCHEMA
const userSchema = new mongoose.Schema({
	
	username: String,
	password: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	
});

// USER MODEL
const User = mongoose.model("User", userSchema);

// POST ROUTE

// Business Logic
/*
	1. Register a username and password
	2. If a duplicate username is found, print duplicate username found
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.post ("/register", (req, res) => {
	User.findOne({username: req.body.username}).then((result, error) => {

		if(result !== null && result.username == req.body.username) {

			return res.send ('Duplicate Username Found');

		} else {

			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, savedDup) => {

				if(savedDup) {

					return console.error (savedDup);
				} else {

					return res.status(201).send("New User Registered");
				}
			})
		}

	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));

// app.post("/task", (req, res) => {
// 	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
// 	// findOne() returns the first document that matches the search criteria as a single object.
// 	Task.findOne({name: req.body.name}).then((result, error) => {

// 		if(result !== null && result.name == req.body.name) {

// 			return res.send('Duplicate Task Found');
// 		}else {
// 			let newTask = new Task ({
// 				name: req.body.name
// 			});
// 			newTask.save().then((savedTask, savedErr) => {

// 				if(savedErr) {
// 					return console.error (savedErr);
// 				} else {
// 					return res.status(201).send("New Task Created");
// 				}
// 			})
// 		}

// 	})
// })